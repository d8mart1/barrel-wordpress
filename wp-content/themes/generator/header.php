<?php
/**
 * The Header template for our theme
 */
 $generator_options = get_option( 'faster_theme_options' );
# print_r($generator_options); ?>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <?php if(!empty($generator_options['favicon'])) { ?>
    <link rel="shortcut icon" href="<?php echo esc_url($generator_options['favicon']);?>">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <?php } ?>
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/respond.min.js"></script>
	<![endif]-->
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<header id="pageheader">
	<div class="upheader">
		<p>Sponsored By</p>
	</div>
	
	<div class="lowheader">
	 <div class="contentwrap">
		<h1><img src="../wp-content/uploads/2018/06/www.knobcreek.com-1311011787501770.png" alt="KNOB" />  </h1>
		<button type="button">LOGO</button>
		<a href="#" class="fa fa-facebook"></a>
        <a href="#" class="fa fa-twitter"></a>
	 </div>
	</div>
     
</header>